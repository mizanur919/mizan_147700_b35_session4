<?php

    namespace bitm8;

    define("something", "something is better than nothing<br><br>");
    define("anything", "something is better than nothing<br><br>");

    echo anything;
    echo something;

    define('b','22.225<br><br>');

    define('some','this is my pen<br><br>');

    echo b;
    echo some;

    define("bitm","this is boolean"." ".true);

    echo bitm."<br><br>";

    echo __LINE__."<br><br>";

    echo __file__."<br><br>";

    echo "Bangladesh is our homeland"." ".__LINE__."<br><br>";

    function someone(){
        echo "this is our homeland"."<br><br>";

        echo __function__."<br><br>";
    }

    someone();

    echo __NAMESPACE__."<br><br>";

    echo __DIR__."<br><br>";

    function again(){
        echo "I live in chittagong"."<br><br>";

        echo __FUNCTION__."<br><br>";

    }

    again();

?>